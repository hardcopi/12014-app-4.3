package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Libraries.Hardware;

@Autonomous (name = "Depot" , group = "ColdBot")
public class BlueDepot extends LinearOpMode {

    Hardware                robot = new Hardware();
    private ElapsedTime     runtime = new ElapsedTime();

    static final double     COUNTS_PER_MOTOR_REV     = 1120;
    static final double     DRIVE_GEAR_REDUCTION     = 1.0;
    static final double     WHEEL_DIAMETER_INCHES    = 3.75;
    static final double     COUNTS_PER_INCH          = (COUNTS_PER_MOTOR_REV * DRIVE_GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);


    @Override
    /**
     * Main op mode for BlueDepot auto program
     *
     */
    public void runOpMode() {

        //Initialize hardware variables
        robot.init(hardwareMap);

        telemetry.addData("Robot Status", "Resetting Encoders");
        telemetry.update();


        robot.frontRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.frontLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.backRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.backLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        robot.frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        //Encoder reset check
        telemetry.addData("Path0", "Starting at %7d :%7d :%7d :%7d",
                robot.frontRight.getCurrentPosition(),
                robot.frontLeft.getCurrentPosition(),
                robot.backRight.getCurrentPosition(),
                robot.backLeft.getCurrentPosition());
        telemetry.update();

        //Wait for starting of autonomous
        waitForStart();

        //Step 1: Turn right
        encoderDrive(0.6, 13, -13, 10);
        sleep(1000);
        //Step 2: Move forward 10 inches
        //move forward
        encoderDrive(0.4, 50, 50, 10);
        sleep(500);

        //Step 3
        robot.teamMarker.setPosition(0);
        sleep(100);
        robot.teamMarker.setPosition(0.7);
        sleep(2000);

        //Step 4
        encoderDrive(0.6, -14, 14, 10);
        sleep(1000);

        //Step 5
        encoderDrive(0.6, 28, 28, 10);

        //Step 4
        encoderDrive(0.6, -2, 2, 10);
        sleep(1000);

        //Step 6
        encoderDrive(1, 36, 36, 10);
    }

    /**
     *  encoderDrive allows the robot to go at a certain speed for a certain distance based
     *  on ticks of the encoder. This allows the robot to be very precise in it's movements
     *
     * @param speed
     * @param leftInches
     * @param rightInches
     * @param timeoutS
     */
        public void encoderDrive(double speed, double leftInches, double rightInches, double timeoutS) {

            int newFrontRightTarget;
            int newFrontLeftTarget;
            int newBackRightTarget;
            int newBackLeftTarget;

            // Ensure that the opmode is still active
            if (opModeIsActive()) {

                // Determine new target position, and pass to motor controller
                newFrontRightTarget = robot.frontRight.getCurrentPosition() + (int)(rightInches * COUNTS_PER_INCH);
                newFrontLeftTarget = robot.frontLeft.getCurrentPosition() + (int)(-leftInches * COUNTS_PER_INCH);
                newBackRightTarget = robot.backRight.getCurrentPosition() + (int)(rightInches * COUNTS_PER_INCH);
                newBackLeftTarget = robot.backLeft.getCurrentPosition() + (int)(-leftInches * COUNTS_PER_INCH);

                robot.frontRight.setTargetPosition(newFrontRightTarget);
                robot.frontLeft.setTargetPosition(newFrontLeftTarget);
                robot.backRight.setTargetPosition(newBackRightTarget);
                robot.backLeft.setTargetPosition(newBackLeftTarget);
                // Turn On RUN_TO_POSITION
                robot.frontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                robot.frontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                robot.backRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                robot.backLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);

                // reset the timeout time and start motion.
                runtime.reset();
                robot.frontRight.setPower(Math.abs(speed));
                robot.frontLeft.setPower(Math.abs(speed));
                robot.backRight.setPower(Math.abs(speed));
                robot.backLeft.setPower(Math.abs(speed));

                // keep looping while we are still active, and there is time left, and both motors are running.
                // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
                // its target position, the motion will stop.  This is "safer" in the event that the robot will
                // always end the motion as soon as possible.
                // However, if you require that BOTH motors have finished their moves before the robot continues
                // onto the next step, use (isBusy() || isBusy()) in the loop test.
                while (opModeIsActive() &&
                        (runtime.seconds() < timeoutS) &&
                        (robot.frontRight.isBusy() && robot.frontLeft.isBusy() && robot.backRight.isBusy() && robot.backLeft.isBusy())) {

                    // Display it for the driver.
                    telemetry.addData("Path1",  "Running to %7d :%7d :%7d :%7d", newFrontRightTarget, newFrontLeftTarget,
                                                                                                newBackRightTarget, newBackLeftTarget);
                    telemetry.addData("Path2",  "Running at %7d :%7d :%7d :%7d",
                            robot.frontRight.getCurrentPosition(),
                            robot.frontLeft.getCurrentPosition(),
                            robot.backRight.getCurrentPosition(),
                            robot.backLeft.getCurrentPosition());
                    telemetry.update();
                }

                // Stop all motion;
                robot.frontRight.setPower(0);
                robot.frontLeft.setPower(0);
                robot.backRight.setPower(0);
                robot.backLeft.setPower(0);


                // Turn off RUN_TO_POSITION
                robot.frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                robot.frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                robot.backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                robot.backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        }
    }
        public void encoderHang(){

            int newHangTarget;

            if (opModeIsActive()) {

                newHangTarget = robot.hangElevator.getCurrentPosition();

                robot.hangElevator.setTargetPosition(newHangTarget);

                robot.hangElevator.setMode(DcMotor.RunMode.RUN_TO_POSITION);

                runtime.reset();
//                robot.hangElevator.setPower(-0.5);

                while (opModeIsActive()) {
                    telemetry.addData("Path1", "Running to %7d", newHangTarget);
                    telemetry.addData("Path2", "Running at %7d", robot.hangElevator.getCurrentPosition());
                    telemetry.update();

                }
            }

    }
}

